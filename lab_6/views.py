from django.shortcuts import render

name = 'Hauri Silmi Zafirah'

def index(request):
	response = {'author': name}
	html = "lab_6/lab_6.html"
	return render(request, html, response)